{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": true,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 210,
  "bbox_top": 0,
  "bbox_bottom": 112,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 211,
  "height": 113,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"155b763e-673f-4c18-b764-10b9f2854883","path":"sprites/spr_tape_ui/spr_tape_ui.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"155b763e-673f-4c18-b764-10b9f2854883","path":"sprites/spr_tape_ui/spr_tape_ui.yy",},"LayerId":{"name":"831d9926-ff80-4c44-9b1a-eb9a3e749653","path":"sprites/spr_tape_ui/spr_tape_ui.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tape_ui","path":"sprites/spr_tape_ui/spr_tape_ui.yy",},"resourceVersion":"1.0","name":"155b763e-673f-4c18-b764-10b9f2854883","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_tape_ui","path":"sprites/spr_tape_ui/spr_tape_ui.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"a08e5ce1-8a1d-4a33-bef6-33d48a594324","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"155b763e-673f-4c18-b764-10b9f2854883","path":"sprites/spr_tape_ui/spr_tape_ui.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 105,
    "yorigin": 56,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_tape_ui","path":"sprites/spr_tape_ui/spr_tape_ui.yy",},
    "resourceVersion": "1.3",
    "name": "spr_tape_ui",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"831d9926-ff80-4c44-9b1a-eb9a3e749653","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "ui",
    "path": "folders/Sprites/dinner_interior/pickable/ui.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_tape_ui",
  "tags": [],
  "resourceType": "GMSprite",
}