{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 1919,
  "bbox_top": 0,
  "bbox_bottom": 1079,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 1920,
  "height": 1080,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"fe29c93e-82d5-41a4-8e10-aa9b71926356","path":"sprites/bg_dinner_interior/bg_dinner_interior.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fe29c93e-82d5-41a4-8e10-aa9b71926356","path":"sprites/bg_dinner_interior/bg_dinner_interior.yy",},"LayerId":{"name":"6813d75d-7324-4bbd-b6f1-b60f6a4b34ac","path":"sprites/bg_dinner_interior/bg_dinner_interior.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"bg_dinner_interior","path":"sprites/bg_dinner_interior/bg_dinner_interior.yy",},"resourceVersion":"1.0","name":"fe29c93e-82d5-41a4-8e10-aa9b71926356","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"bg_dinner_interior","path":"sprites/bg_dinner_interior/bg_dinner_interior.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"1b245088-cf46-487a-b4b2-7bb2512b2a02","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fe29c93e-82d5-41a4-8e10-aa9b71926356","path":"sprites/bg_dinner_interior/bg_dinner_interior.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 960,
    "yorigin": 540,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"bg_dinner_interior","path":"sprites/bg_dinner_interior/bg_dinner_interior.yy",},
    "resourceVersion": "1.3",
    "name": "bg_dinner_interior",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"6813d75d-7324-4bbd-b6f1-b60f6a4b34ac","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "backs",
    "path": "folders/Sprites/backs.yy",
  },
  "resourceVersion": "1.0",
  "name": "bg_dinner_interior",
  "tags": [],
  "resourceType": "GMSprite",
}