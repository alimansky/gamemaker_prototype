{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 374,
  "bbox_top": 0,
  "bbox_bottom": 261,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 375,
  "height": 262,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"be4b0103-0558-45ac-a8a2-05beee8d4335","path":"sprites/spr_recorder_without_tape_trigger/spr_recorder_without_tape_trigger.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"be4b0103-0558-45ac-a8a2-05beee8d4335","path":"sprites/spr_recorder_without_tape_trigger/spr_recorder_without_tape_trigger.yy",},"LayerId":{"name":"8354f918-6adb-41be-adcf-5cb7bbb0b3d2","path":"sprites/spr_recorder_without_tape_trigger/spr_recorder_without_tape_trigger.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_recorder_without_tape_trigger","path":"sprites/spr_recorder_without_tape_trigger/spr_recorder_without_tape_trigger.yy",},"resourceVersion":"1.0","name":"be4b0103-0558-45ac-a8a2-05beee8d4335","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_recorder_without_tape_trigger","path":"sprites/spr_recorder_without_tape_trigger/spr_recorder_without_tape_trigger.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"0f306e8f-16c2-4a18-b8a4-f77320398109","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"be4b0103-0558-45ac-a8a2-05beee8d4335","path":"sprites/spr_recorder_without_tape_trigger/spr_recorder_without_tape_trigger.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 187,
    "yorigin": 131,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_recorder_without_tape_trigger","path":"sprites/spr_recorder_without_tape_trigger/spr_recorder_without_tape_trigger.yy",},
    "resourceVersion": "1.3",
    "name": "spr_recorder_without_tape_trigger",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"8354f918-6adb-41be-adcf-5cb7bbb0b3d2","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "triggers",
    "path": "folders/Sprites/dinner_interior/triggers.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_recorder_without_tape_trigger",
  "tags": [],
  "resourceType": "GMSprite",
}