{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": true,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 3,
  "bbox_right": 124,
  "bbox_top": 5,
  "bbox_bottom": 122,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 128,
  "height": 128,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"ee57cfff-9c2f-4038-b062-cca46ed59392","path":"sprites/spr_pickitem/spr_pickitem.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ee57cfff-9c2f-4038-b062-cca46ed59392","path":"sprites/spr_pickitem/spr_pickitem.yy",},"LayerId":{"name":"1882e482-db9e-4d13-a0ef-91b4dcdc9e26","path":"sprites/spr_pickitem/spr_pickitem.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_pickitem","path":"sprites/spr_pickitem/spr_pickitem.yy",},"resourceVersion":"1.0","name":"ee57cfff-9c2f-4038-b062-cca46ed59392","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_pickitem","path":"sprites/spr_pickitem/spr_pickitem.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"34892e1e-2e65-4229-8d85-fa3a67916758","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ee57cfff-9c2f-4038-b062-cca46ed59392","path":"sprites/spr_pickitem/spr_pickitem.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 64,
    "yorigin": 64,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_pickitem","path":"sprites/spr_pickitem/spr_pickitem.yy",},
    "resourceVersion": "1.3",
    "name": "spr_pickitem",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"1882e482-db9e-4d13-a0ef-91b4dcdc9e26","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "ui",
    "path": "folders/Sprites/ui.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_pickitem",
  "tags": [],
  "resourceType": "GMSprite",
}