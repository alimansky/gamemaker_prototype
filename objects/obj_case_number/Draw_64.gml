draw_set_font(fnt_car_case);
draw_set_color(c_black);
draw_set_halign(fa_center);
draw_set_valign(fa_center);
draw_text_transformed(x, y, string_hash_to_newline(string(number)), image_xscale, image_yscale, image_angle);

