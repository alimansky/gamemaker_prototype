if picked == true
{
    var target = (global.sel == id);
    if target depth = -2; else depth = -1;
    scale = move_towards(scale, target, abs(target-scale)/10);
    draw_sprite_ext(sprite_index, image_index, x, y, image_xscale+0.2*scale, image_yscale+0.2*scale, image_angle, image_blend, image_alpha);
}

