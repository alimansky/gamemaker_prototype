/// @description On click

if PAUSE exit;

if obj != -1 and (global.sel).object_index != obj 
{
    geon_effect_burst(global._pt_wrong_move, mouse_x, mouse_y);
	snd_play_ext(snd_error);
    exit;
}
else
{   	
	geon_effect_burst(global._pt_right_move, mouse_x, mouse_y);
	
    if remove
    {
        with global.sel remove_item(other.id);
    }
    else
    {
        with global.sel return_item(other.id);
    }   
}

