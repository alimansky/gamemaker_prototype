/*
 This is all for draggin porpouses
 We set the distance between this step and the step before, then
 we add this value to the offset from the item list
*/


if mouse_check_button_pressed(mb_left) 
{
    mouse_xprevious = device_mouse_raw_x(0);
    if device_mouse_x_to_gui(0) > xmin-40 and device_mouse_y_to_gui(0) > GUI_H-panel_height and !PAUSE
    {
        dragging = 1;
    }
    else dragging = 0;
}

var mb = mouse_check_button(mb_left);
if mb
{
    if dragging 
    {
        dragx = device_mouse_raw_x(0)-mouse_xprevious;
        if dragx > 5 button_pressed = 0;
    }
}
else 
{
    dragging = 0;
}

if !dragging dragx *= 0.95;
offset += dragx;
mouse_xprevious = device_mouse_raw_x(0);

if !mb or (mb and dragging = 0)
{
    if offset > xmin {dragx = 0; offset = move_towards(offset, xmin, max(1, ceil(abs(offset - xmin) * 0.1)));}
    
    var vwidth = panel_width;//GUI_W-xmin;    
    
    if width > vwidth 
    {
        var maxpos = xmin - (width - vwidth);
        if offset < maxpos {dragx = 0; offset = move_towards(offset, maxpos, max(1, ceil(abs(offset - maxpos) * 0.1)));}
    }
    else {dragx = 0; offset = move_towards(offset, xmin, max(1, ceil(abs(offset - xmin) * 0.1)));}
}