ds_list_clear(global.inv_list);

// Variables declarations
width = 0;
dragx = 0;
sidebar_alpha = 0;
mouse_xprevious = mouse_x;
pressed = noone;
released = 1000000;


xindent = 160;
xmin = (sprite_get_width(spr_map_button)-20) + xindent;//x - panel_width / 2 + item_width / 2;
offset = xmin;

panel_width = GUI_W - (sprite_get_width(spr_map_button)-20) - (sprite_get_width(spr_hint_button)-20) - xindent - 40;//sprite_get_width(spr_inventory) - sprite_get_width(spr_arrow_button) * 2;
panel_height = background_get_height(bg_inv_tile);

item_width = panel_height-8;
item_height = panel_height;

children = 0;

