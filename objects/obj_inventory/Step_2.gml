// Set the starting x offset and width
var xx = offset;
var yy = GUI_H - panel_height / 2;
width = 0;
selected_item = -1;

var mx = device_mouse_x_to_gui(0);
var my = device_mouse_y_to_gui(0);

var item_num = ds_list_size(global.inv_list);

for (var i = 0; i < item_num; i += 1) 
{  
    var ii = 0;
    
    if point_in_rectangle(mx, my, xx-item_width/2, yy-item_height/2, xx+item_width/2, yy+item_height/2) 
    {
        if (mouse_check_button(mb_left)) ii = 1; // Item Hover
        if (mouse_check_button_pressed(mb_left)) pressed = mx;
        if (mouse_check_button_released(mb_left)) released = mx;
    }
    
    var ins = ds_list_find_value(global.inv_list, i);
    if instance_exists(ins)
    {
        with ins 
        {
            if in_pocket
            {
                var sp = point_distance(x, y, xx, yy) * 0.1;
                move_towards_point(xx, yy, sp);
                //move_towards(offset, xmin, max(1, ceil(abs(offset - xmin) * 0.1)
                //x = xx;
                //y = yy;
            }
        }
    }

    xx += item_width;
    width += item_width; 
    
    if (point_distance(pressed, 0, released, 0) < 10) 
    {
        selected_item = i;
        released = -99999;
    }  
}

released = -9999;

if (selected_item != -1) 
{
    var ins_sel = ds_list_find_value(global.inv_list, selected_item);
    if instance_exists(ins_sel) and !PAUSE and mx >= xmin-40 and mx < xmin+panel_width-60 
    {
        if global.sel != ins_sel global.sel = ins_sel;
        else global.sel = -1;
    }
}

