if(global.puzzleRecorderRewindTape == true)
{	
	audio_play_sound(snd_RecordDiana, 10, false);
	obj_ring.visible = 1;
	
	global.puzzleRecorderRewindTape = false;
	
	geon_effect_burst(global._pt_right_move, mouse_x, mouse_y);
}
else if (global.puzzleRecorderGetTape == false && global.puzzleRecorderRewindTape == false)
{
	audio_play_sound(snd_ClickRecorder, 10, false);
    geon_effect_burst(global._pt_wrong_move, mouse_x, mouse_y);
    exit;
}