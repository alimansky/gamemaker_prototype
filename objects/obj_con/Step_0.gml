//if PAUSE exit;
if mouse_y >= __view_get( e__VW.YView, 0 )+__view_get( e__VW.HView, 0 )-background_get_height(bg_inv_tile) exit;

var ins;

if mouse_check_button_pressed(mb_left)
{
    ins = position_get_top_instance(mouse_x, mouse_y, obj_clickable);

    if ins != noone
    {
        pressed = ins;
    }
}   

if mouse_check_button_released(mb_left)
{
    //ins = position_get_top_instance(mouse_x, mouse_y, obj_clickable);
	if instance_exists(pressed)
	{
	    if position_meeting(mouse_x, mouse_y, pressed)
	    {
	        with pressed event_perform(ev_other, ev_user0);
	        pressed = noone;
	    }
	}
}

        

//gui_button_check();

