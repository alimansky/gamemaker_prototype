var dx,dy;

dx = hook_x-x;
dy = hook_y-y;
m = point_distance(0, 0, dx, dy);

if m > length
{
    m/=(m-length);
    m*=elastic;
    dx/=m;
    dy/=m;
    x+=dx;
    y+=dy;
    hspeed+=dx;
    vspeed+=dy;
}

//draw_line_width(x,y,hook.x,hook.y,3);
image_angle = point_direction(x, y, hook_x, hook_y)-90;

with obj_sticky_note
{
    if fallen = 0
    {
        var ang = other.image_angle;
        x = other.x;
        y = other.y;
        image_angle = ang;
        if abs(ang) >= 60
        {
            note_fall();
        }
    }
}

