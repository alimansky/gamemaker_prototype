if (global.puzzleRecorderGetTape == false)
{
	global.puzzleRecorderGetTape = true;

	obj_recorder_tape.visible = false;	
	obj_tape.visible = true;
	
	obj_recorder.visible = 0;
	instance_deactivate_object(obj_recorder);	
	
	instance_activate_object(obj_recorder_without_tape);
	obj_recorder_without_tape.visible = 1;
	
	audio_play_sound(snd_ClickRecorder, 10, false);
}