if (counter == 30 && room = rm_diner_front)
{    
	audio_play_sound(snd_Walk_LeftFoot, 1, false);

	for (var i = 1; i < 5; i++) 
	{
		// Wait for the first sound to finish
		if (i mod 2 != 0)
		{
			while (audio_is_playing(snd_Walk_LeftFoot)) {}
			// Play the next   
			audio_play_sound(snd_Walk_RightFoot, 1, false);  
		}
		else
		{
			while (audio_is_playing(snd_Walk_RightFoot)) {}
			// Play the next   
			audio_play_sound(snd_Walk_LeftFoot, 1, false);  
		}
	}
	
	while (audio_is_playing(snd_Walk_RightFoot)) {}
	
	snd_play_ext(snd_openDoor, 1);
	
	while (audio_is_playing(snd_openDoor)) {}
	
	counter = 31;
		
	room_goto_next();
	
	counter = 0;
}
else
{
	counter++;
}