action_inherited();

children = 0;

child_add(740 - GUI_W/2, 492 - GUI_H/2, obj_recorder_get_tape_button);
child_add(784 - GUI_W/2, 263.5 - GUI_H/2, obj_recorder_play_button);

if(global.puzzleRecorderGetTape == false || global.puzzleRecorderRewindTape == true)
	child_add(738 - GUI_W/2, 594 - GUI_H/2, obj_recorder_tape);

child_add(574 - GUI_W/2, 269.5 - GUI_H/2, obj_recorder_pause_button);
child_add(893 - GUI_W/2, 263.5 - GUI_H/2, obj_recorder_speedup_button);
child_add(975 - GUI_W/2, 263.5 - GUI_H/2, obj_recorder_speeddown_button);

if(instance_exists(obj_recorder_get_tape_button))
	obj_recorder_get_tape_button.depth = -99;
	
if(instance_exists(obj_recorder_tape))	
	obj_recorder_tape.depth = -99;