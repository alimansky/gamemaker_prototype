var settingsLayerId = layer_get_id("settings");
var menuLayerId = layer_get_id("menu");

layer_set_visible(menuLayerId, false);
layer_set_visible(settingsLayerId, true);