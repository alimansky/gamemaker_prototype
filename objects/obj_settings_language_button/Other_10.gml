/// @description Click here

if(global.language < 9)
	global.language += 1;
else 
	global.language = 0;

// TODO asian characters in font :'(
{
	if(global.language == lang.jp || global.language == lang.kor || global.language == lang.ch)
		global.language += 1;
	
	if(global.language >= 9) 
		global.language = 0;
}

set_font();

// loc load
loc_load(global.language, "loc//loc.tsv");