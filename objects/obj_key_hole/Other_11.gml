    instance_destroy();
    with obj_lighter_slot on = 1;
    with obj_lighter 
    {
        if !picked and image_index = 0
        {
             image_index = 1;
             cheer_up();
        }
    }

