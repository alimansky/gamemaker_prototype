{
  "spriteId": {
    "name": "spr_key_hole",
    "path": "sprites/spr_key_hole/spr_key_hole.yy",
  },
  "solid": false,
  "visible": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": {
    "name": "obj_usable",
    "path": "objects/obj_usable/obj_usable.yy",
  },
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 0,
  "physicsGroup": 0,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":0,"collisionObjectId":null,"parent":{"name":"obj_key_hole","path":"objects/obj_key_hole/obj_key_hole.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":11,"eventType":7,"collisionObjectId":null,"parent":{"name":"obj_key_hole","path":"objects/obj_key_hole/obj_key_hole.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "usable",
    "path": "folders/Objects/car_interior/usable.yy",
  },
  "resourceVersion": "1.0",
  "name": "obj_key_hole",
  "tags": [],
  "resourceType": "GMObject",
}