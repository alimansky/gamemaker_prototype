draw_set_halign(fa_left);
draw_set_valign(fa_center);
draw_set_font(global.fnt_menu);
var col; 
col[0] = c_gray;
col[1] = c_white;
draw_set_color(col[hover]);
draw_self();

if(music_enabled == 1)
	draw_text(x - 125, y, loc("UI_ENABLED_BUTTON"));
else
	draw_text(x - 125, y, loc("UI_DISABLED_BUTTON"));