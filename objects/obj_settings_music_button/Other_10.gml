if(music_enabled == 1)
{
	music_enabled = 0;
	audio_master_gain(0);
}
else
{
	music_enabled = 1;
	audio_master_gain(global.volume);
}