{
  "spriteId": {
    "name": "spr_coffee_cup",
    "path": "sprites/spr_coffee_cup/spr_coffee_cup.yy",
  },
  "solid": false,
  "visible": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": {
    "name": "obj_prop",
    "path": "objects/obj_prop/obj_prop.yy",
  },
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 0,
  "physicsGroup": 0,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":0,"collisionObjectId":null,"parent":{"name":"obj_coffee_cup","path":"objects/obj_coffee_cup/obj_coffee_cup.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":10,"eventType":7,"collisionObjectId":null,"parent":{"name":"obj_coffee_cup","path":"objects/obj_coffee_cup/obj_coffee_cup.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "props",
    "path": "folders/Objects/dinner_interior/props.yy",
  },
  "resourceVersion": "1.0",
  "name": "obj_coffee_cup",
  "tags": [],
  "resourceType": "GMObject",
}