//if room = rm_car_interior scene_read(SCENE_DIR+"car_interior.txt");
// change cursor
window_set_cursor(cr_none);
cursor_sprite = spr_cursor;

if(instance_exists(obj_tape_pickable))
{
	obj_tape_pickable.visible = 0;
	instance_deactivate_object(obj_tape_pickable);
}

if(instance_exists(obj_recorder_without_tape))
{
	obj_recorder_without_tape.visible = 0;
	instance_deactivate_object(obj_recorder_without_tape);
}

if(instance_exists(obj_tape))
	obj_tape.visible = 0;

if(instance_exists(obj_ring))
	obj_ring.visible = 0;
	
if !audio_is_playing(snd_MainTheme) audio_play_sound(snd_MainTheme, 1, 1);	