TweenFire(id, image_xscale__, EaseLinear, TWEEN_MODE_BOUNCE, 1, 0, 0.1, 1, 1.2);

if room == rm_car_interior 
{
	show_message(loc("HINT_LOCATION_1"));
}

if room = rm_diner_front
{
	show_message(loc("HINT_LOCATION_2"));
}

if room == rm_dinner_interior 
{
	show_message(loc("HINT_LOCATION_3"));
}