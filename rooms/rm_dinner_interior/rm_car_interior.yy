{
  "isDnd": false,
  "volume": 1.0,
  "parentRoom": null,
  "views": [
    {"inherit":false,"visible":true,"xview":0,"yview":0,"wview":1920,"hview":1080,"xport":0,"yport":0,"wport":1920,"hport":1080,"hborder":32,"vborder":32,"hspeed":-1,"vspeed":-1,"objectId":null,},
    {"inherit":false,"visible":false,"xview":0,"yview":0,"wview":1024,"hview":768,"xport":0,"yport":0,"wport":1024,"hport":768,"hborder":32,"vborder":32,"hspeed":-1,"vspeed":-1,"objectId":null,},
    {"inherit":false,"visible":false,"xview":0,"yview":0,"wview":1024,"hview":768,"xport":0,"yport":0,"wport":1024,"hport":768,"hborder":32,"vborder":32,"hspeed":-1,"vspeed":-1,"objectId":null,},
    {"inherit":false,"visible":false,"xview":0,"yview":0,"wview":1024,"hview":768,"xport":0,"yport":0,"wport":1024,"hport":768,"hborder":32,"vborder":32,"hspeed":-1,"vspeed":-1,"objectId":null,},
    {"inherit":false,"visible":false,"xview":0,"yview":0,"wview":1024,"hview":768,"xport":0,"yport":0,"wport":1024,"hport":768,"hborder":32,"vborder":32,"hspeed":-1,"vspeed":-1,"objectId":null,},
    {"inherit":false,"visible":false,"xview":0,"yview":0,"wview":1024,"hview":768,"xport":0,"yport":0,"wport":1024,"hport":768,"hborder":32,"vborder":32,"hspeed":-1,"vspeed":-1,"objectId":null,},
    {"inherit":false,"visible":false,"xview":0,"yview":0,"wview":1024,"hview":768,"xport":0,"yport":0,"wport":1024,"hport":768,"hborder":32,"vborder":32,"hspeed":-1,"vspeed":-1,"objectId":null,},
    {"inherit":false,"visible":false,"xview":0,"yview":0,"wview":1024,"hview":768,"xport":0,"yport":0,"wport":1024,"hport":768,"hborder":32,"vborder":32,"hspeed":-1,"vspeed":-1,"objectId":null,},
  ],
  "layers": [
    {"assets":[
        {"spriteId":{"name":"spr_car_light","path":"sprites/spr_car_light/spr_car_light.yy",},"headPosition":0.0,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"animationSpeed":1.0,"colour":4294967295,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":960.0,"y":96.0,"resourceVersion":"1.0","name":"graphic_7AA9939A","tags":[],"resourceType":"GMRSpriteGraphic",},
      ],"visible":false,"depth":0,"userdefinedDepth":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"gridX":32,"gridY":32,"layers":[],"hierarchyFrozen":false,"resourceVersion":"1.0","name":"car_light","tags":[],"resourceType":"GMRAssetLayer",},
    {"instances":[
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_car_radio","path":"objects/obj_car_radio/obj_car_radio.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":957.0,"y":471.0,"resourceVersion":"1.0","name":"inst_1643E0E0","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_key_start","path":"objects/obj_key_start/obj_key_start.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":332.0,"y":418.0,"resourceVersion":"1.0","name":"inst_6DD599A3","tags":[],"resourceType":"GMRInstance",},
        {"properties":[
            {"propertyId":{"name":"tooltip_color","path":"objects/obj_paper_cup/obj_paper_cup.yy",},"objectId":{"name":"obj_paper_cup","path":"objects/obj_paper_cup/obj_paper_cup.yy",},"value":"$00278AB5","resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMOverriddenProperty",},
          ],"isDnd":false,"objectId":{"name":"obj_paper_cup","path":"objects/obj_paper_cup/obj_paper_cup.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":435.0,"y":372.0,"resourceVersion":"1.0","name":"inst_44B04D25","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_lighter_slot","path":"objects/obj_lighter_slot/obj_lighter_slot.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":960.0,"y":655.0,"resourceVersion":"1.0","name":"inst_63DA54BD","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_lighter","path":"objects/obj_lighter/obj_lighter.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":963.0,"y":652.0,"resourceVersion":"1.0","name":"inst_5FFA1825","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_pedal","path":"objects/obj_pedal/obj_pedal.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":660.0,"y":779.0,"resourceVersion":"1.0","name":"inst_5233AD1C","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_key_hole","path":"objects/obj_key_hole/obj_key_hole.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":779.0,"y":675.0,"resourceVersion":"1.0","name":"inst_416F8C39","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_steering_wheel","path":"objects/obj_steering_wheel/obj_steering_wheel.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":575.0,"y":566.0,"resourceVersion":"1.0","name":"inst_7CF67577","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_notebook","path":"objects/obj_notebook/obj_notebook.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1300.0,"y":617.0,"resourceVersion":"1.0","name":"inst_BD51ED5","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_id","path":"objects/obj_id/obj_id.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1375.0,"y":608.0,"resourceVersion":"1.0","name":"inst_BAECF40","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_gun","path":"objects/obj_gun/obj_gun.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1459.0,"y":624.0,"resourceVersion":"1.0","name":"inst_2A187A0F","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_glove_compartment","path":"objects/obj_glove_compartment/obj_glove_compartment.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1379.0,"y":665.0,"resourceVersion":"1.0","name":"inst_7381ACCF","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_case","path":"objects/obj_case/obj_case.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1435.0,"y":890.0,"resourceVersion":"1.0","name":"inst_7360F0F0","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_folder","path":"objects/obj_folder/obj_folder.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1318.0,"y":970.0,"resourceVersion":"1.0","name":"inst_2124A4FF","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_light_button","path":"objects/obj_light_button/obj_light_button.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":960.0,"y":74.0,"resourceVersion":"1.0","name":"inst_6ABCC072","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_sticky_note","path":"objects/obj_sticky_note/obj_sticky_note.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":944.0,"y":389.0,"resourceVersion":"1.0","name":"inst_403B57FD","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_air_freshener","path":"objects/obj_air_freshener/obj_air_freshener.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":961.0,"y":263.0,"resourceVersion":"1.0","name":"inst_205012D5","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_mirror","path":"objects/obj_mirror/obj_mirror.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":972.0,"y":158.0,"resourceVersion":"1.0","name":"inst_18BC395D","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_leaflets","path":"objects/obj_leaflets/obj_leaflets.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1034.0,"y":397.0,"resourceVersion":"1.0","name":"inst_9999D01","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_stuff","path":"objects/obj_stuff/obj_stuff.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1381.0,"y":342.0,"resourceVersion":"1.0","name":"inst_4E0AFB82","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_key_glove_compartment","path":"objects/obj_key_glove_compartment/obj_key_glove_compartment.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":686.0,"y":816.0,"resourceVersion":"1.0","name":"inst_AB6ABF","tags":[],"resourceType":"GMRInstance",},
      ],"visible":true,"depth":100,"userdefinedDepth":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"gridX":1,"gridY":1,"layers":[],"hierarchyFrozen":false,"resourceVersion":"1.0","name":"items","tags":[],"resourceType":"GMRInstanceLayer",},
    {"instances":[
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_con","path":"objects/obj_con/obj_con.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":0.0,"y":0.0,"resourceVersion":"1.0","name":"inst_F85C189A","tags":[],"resourceType":"GMRInstance",},
      ],"visible":true,"depth":10000000,"userdefinedDepth":true,"inheritLayerDepth":false,"inheritLayerSettings":false,"gridX":1,"gridY":1,"layers":[],"hierarchyFrozen":false,"resourceVersion":"1.0","name":"cons","tags":[],"resourceType":"GMRInstanceLayer",},
    {"spriteId":{"name":"bg_car_interior","path":"sprites/bg_car_interior/bg_car_interior.yy",},"colour":4294967295,"x":0,"y":0,"htiled":true,"vtiled":true,"hspeed":0.0,"vspeed":0.0,"stretch":false,"animationFPS":1.0,"animationSpeedType":1,"userdefinedAnimFPS":false,"visible":true,"depth":2147483500,"userdefinedDepth":true,"inheritLayerDepth":false,"inheritLayerSettings":false,"gridX":32,"gridY":32,"layers":[],"hierarchyFrozen":false,"resourceVersion":"1.0","name":"bg_car","tags":[],"resourceType":"GMRBackgroundLayer",},
  ],
  "inheritLayers": false,
  "creationCodeFile": "",
  "inheritCode": false,
  "instanceCreationOrder": [
    {"name":"inst_F85C189A","path":"rooms/rm_car_interior/rm_car_interior.yy",},
    {"name":"inst_1643E0E0","path":"rooms/rm_car_interior/rm_car_interior.yy",},
    {"name":"inst_6DD599A3","path":"rooms/rm_car_interior/rm_car_interior.yy",},
    {"name":"inst_44B04D25","path":"rooms/rm_car_interior/rm_car_interior.yy",},
    {"name":"inst_63DA54BD","path":"rooms/rm_car_interior/rm_car_interior.yy",},
    {"name":"inst_5FFA1825","path":"rooms/rm_car_interior/rm_car_interior.yy",},
    {"name":"inst_7CF67577","path":"rooms/rm_car_interior/rm_car_interior.yy",},
    {"name":"inst_5233AD1C","path":"rooms/rm_car_interior/rm_car_interior.yy",},
    {"name":"inst_7360F0F0","path":"rooms/rm_car_interior/rm_car_interior.yy",},
    {"name":"inst_2124A4FF","path":"rooms/rm_car_interior/rm_car_interior.yy",},
    {"name":"inst_7381ACCF","path":"rooms/rm_car_interior/rm_car_interior.yy",},
    {"name":"inst_BAECF40","path":"rooms/rm_car_interior/rm_car_interior.yy",},
    {"name":"inst_BD51ED5","path":"rooms/rm_car_interior/rm_car_interior.yy",},
    {"name":"inst_2A187A0F","path":"rooms/rm_car_interior/rm_car_interior.yy",},
    {"name":"inst_6ABCC072","path":"rooms/rm_car_interior/rm_car_interior.yy",},
    {"name":"inst_18BC395D","path":"rooms/rm_car_interior/rm_car_interior.yy",},
    {"name":"inst_205012D5","path":"rooms/rm_car_interior/rm_car_interior.yy",},
    {"name":"inst_403B57FD","path":"rooms/rm_car_interior/rm_car_interior.yy",},
    {"name":"inst_9999D01","path":"rooms/rm_car_interior/rm_car_interior.yy",},
    {"name":"inst_416F8C39","path":"rooms/rm_car_interior/rm_car_interior.yy",},
    {"name":"inst_AB6ABF","path":"rooms/rm_car_interior/rm_car_interior.yy",},
    {"name":"inst_4E0AFB82","path":"rooms/rm_car_interior/rm_car_interior.yy",},
  ],
  "inheritCreationOrder": false,
  "sequenceId": null,
  "roomSettings": {
    "inheritRoomSettings": false,
    "Width": 1920,
    "Height": 1080,
    "persistent": false,
  },
  "viewSettings": {
    "inheritViewSettings": false,
    "enableViews": true,
    "clearViewBackground": true,
    "clearDisplayBuffer": true,
  },
  "physicsSettings": {
    "inheritPhysicsSettings": false,
    "PhysicsWorld": false,
    "PhysicsWorldGravityX": 0.0,
    "PhysicsWorldGravityY": 10.0,
    "PhysicsWorldPixToMetres": 0.1,
  },
  "parent": {
    "name": "Rooms",
    "path": "folders/Rooms.yy",
  },
  "resourceVersion": "1.0",
  "name": "rm_car_interior",
  "tags": [],
  "resourceType": "GMRoom",
}