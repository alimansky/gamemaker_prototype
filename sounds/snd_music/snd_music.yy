{
  "compression": 3,
  "volume": 0.8,
  "preload": false,
  "bitRate": 192,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_music.ogg",
  "duration": 235.137268,
  "parent": {
    "name": "Sound",
    "path": "folders/Sound.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_music",
  "tags": [],
  "resourceType": "GMSound",
}