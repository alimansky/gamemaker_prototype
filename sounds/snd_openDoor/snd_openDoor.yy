{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_openDoor.wav",
  "duration": 0.840918,
  "parent": {
    "name": "Sound",
    "path": "folders/Sound.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_openDoor",
  "tags": [],
  "resourceType": "GMSound",
}