{
  "compression": 1,
  "volume": 0.3,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_use.wav",
  "duration": 1.505,
  "parent": {
    "name": "Sound",
    "path": "folders/Sound.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_use",
  "tags": [],
  "resourceType": "GMSound",
}