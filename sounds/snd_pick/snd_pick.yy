{
  "compression": 1,
  "volume": 0.3,
  "preload": false,
  "bitRate": 192,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_pick.wav",
  "duration": 1.611712,
  "parent": {
    "name": "Sound",
    "path": "folders/Sound.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_pick",
  "tags": [],
  "resourceType": "GMSound",
}