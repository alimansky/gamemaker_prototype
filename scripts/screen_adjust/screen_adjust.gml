function screen_adjust() {
	application_surface_enable(false);
	application_surface_draw_enable(false);
	display_set_timing_method(tm_sleep);
	
	globalvar DW, DH, BASE_W, BASE_H; 
	BASE_W = 1920;
	BASE_H = 1080; 
	DW = floor(display_get_width());
	DH = floor(display_get_height());

	if os_type == os_windows || os_type == os_linux || os_type == os_macosx
	{
		// #TODO
	    DW = 1920;//1024//1920;
	    DH = 1080;//768//1080;
	    window_set_size(DW, DH);   
	    window_set_fullscreen(0);
	    //window_set_position(400, 100);
	    //DW = 960//1024//1920;
	    //DH = 640//768//1080;
	    //window_set_size(DW, DH);   
	    //window_set_fullscreen(0);
	    //window_set_position(400, 100);
	}

	var aspect = DW / DH;
	var view_h = BASE_H;
	var view_w = view_h * aspect;

	__view_set( e__VW.WView, 0, floor(view_w) );
	__view_set( e__VW.HView, 0, floor(view_h) );

	var xview, yview;
	xview = BASE_W/2-__view_get( e__VW.WView, 0 )/2;
	yview = BASE_H/2-__view_get( e__VW.HView, 0 )/2;

	__view_set( e__VW.XView, 0, xview );
	__view_set( e__VW.YView, 0, yview );

	__view_set( e__VW.WPort, 0, DW );
	__view_set( e__VW.HPort, 0, DH );

	var rm_ind = room_first;
	do 
	{
	    room_set_view(rm_ind, 0, 1, xview, yview, __view_get( e__VW.WView, 0 ), __view_get( e__VW.HView, 0 ), 0, 0, __view_get( e__VW.WPort, 0 ), __view_get( e__VW.HPort, 0 ), 0, 0, 0, 0, -1);
	    rm_ind = room_next(rm_ind);
	}
	until rm_ind = -1;

	globalvar GUI_W, GUI_H;
	GUI_W = __view_get( e__VW.WView, 0 );
	GUI_H = __view_get( e__VW.HView, 0 );
	display_set_gui_size(GUI_W, GUI_H);
	surface_resize(application_surface, __view_get( e__VW.WView, 0 ), __view_get( e__VW.HView, 0 ));

	window_set_color(c_black);



}
