/// @description  TweenDSList(target,ease,mode,delta,delay,dur,list,index,start,dest)
/// @param target
/// @param ease
/// @param mode
/// @param delta
/// @param delay
/// @param dur
/// @param list
/// @param index
/// @param start
/// @param dest
function TweenDSList(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7, argument8, argument9) {

	return TweenFire(argument0, ext_DSList__, argument1, argument2, argument3, argument4, argument5, argument8, argument9, argument6, argument7);




}
