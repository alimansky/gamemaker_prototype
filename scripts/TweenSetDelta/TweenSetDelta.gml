/// @description  TweenSetDelta(tween,bool)
/// @param tween
/// @param bool
function TweenSetDelta(argument0, argument1) {

	var _t = TGMS_FetchTween(argument0);
	if (is_undefined(_t)) return 0;

	_t[@ TWEEN.DELTA] = argument1;





}
