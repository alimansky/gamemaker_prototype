function gui_create() {
	var inv_height = background_get_height(bg_inv_tile)/2;

	if instance_exists(obj_inventory) exit;
	instance_create(GUI_W/2, GUI_H-inv_height, obj_inventory);
	
	var map_w = sprite_get_xoffset(spr_map_button);
	var hint_w = sprite_get_xoffset(spr_hint_button);

	instance_create(map_w-20, GUI_H-sprite_get_yoffset(spr_map_button), obj_map_button);
	instance_create(GUI_W-hint_w+20, GUI_H-sprite_get_yoffset(spr_hint_button), obj_hint_button);

	instance_create(-50+map_w*2+sprite_get_xoffset(spr_arrow_button), GUI_H-inv_height, obj_left_arrow_button);
	instance_create(50+GUI_W-hint_w*2-sprite_get_xoffset(spr_arrow_button), GUI_H-inv_height, obj_right_arrow_button);

}
