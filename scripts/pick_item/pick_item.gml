function pick_item() 
{
	snd_play_ext(snd_pick);
	//audio_play_sound(snd_pick, 1, 0);
	depth_init = depth;
	sprite_init = sprite_index;

	var spr_name = sprite_get_name(sprite_index);
	var ind = asset_get_index(spr_name + "_ui");
	if ind > -1
	{
	    if sprite_exists(ind) sprite_index = ind;
	}

	x = x-__view_get( e__VW.XView, 0 )//device_mouse_x_to_gui(0);
	y = y-__view_get( e__VW.YView, 0 )//device_mouse_y_to_gui(0);
	depth = -3;
	picked = 1;

	var item_name = id; //string_replace(object_get_name(object_index), "obj_", "");
	ds_list_add(global.inv_list, item_name);
	item_pos = ds_list_size(global.inv_list)-1;

	var t_goto_center = TweenSimpleMove(x, y, GUI_W/2, GUI_H/2, 0.5, EaseOutExpo);
	TweenSimpleScale(0.3, 0.3, 1.7, 1.7, 0.5, EaseOutExpo);
	TweenAddCallback(t_goto_center, TWEEN_EV_FINISH, id, pocket_item);
	
	geon_effect_burst(global._pt_pickitem, mouse_x, mouse_y);
}
