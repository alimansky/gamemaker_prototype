/// @description  TweenDefaultDestroyWhenDone(**Deprecated**destroy)
/// @param **Deprecated**destroy
function TweenDefaultDestroyWhenDone(argument0) {
	/*
	    Indicate if created tweens should be destroyed by default when done
	*/

	global.TGMS_TweenDefault[@ TWEEN.DESTROY] = argument0;



}
