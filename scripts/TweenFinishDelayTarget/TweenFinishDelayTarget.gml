/// @description  TweenFinishDelayTarget(target,call_event,deactivated)
/// @param target
/// @param call_event
/// @param deactivated
function TweenFinishDelayTarget(argument0, argument1, argument2) {

	TweensExecute(TWEENS_TARGET, argument0, argument2, TweenFinishDelay, argument1);



}
