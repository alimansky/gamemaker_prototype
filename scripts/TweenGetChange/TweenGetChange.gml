/// @description  TweenGetChange(tween)
/// @param tween
function TweenGetChange(argument0) {

	var _t = TGMS_FetchTween(argument0);
	if (is_undefined(_t)) return 0;

	return _t[TWEEN.CHANGE];




}
