function pocket_item() {
	with obj_inventory
	{
	    if width > panel_width
	    {
	        var maxpos = xmin - (width - panel_width);
	        offset = maxpos;
	    }
	}

	xp = obj_inventory.offset + obj_inventory.item_width * item_pos //+ obj_inventory.item_width/2;
	yp = obj_inventory.y;
	var take_item = TweenSimpleMove(x, y, xp, yp, 0.5, EaseInExpo, 0);
	TweenSimpleScale(1.7, 1.7, 0.28, 0.28, 0.5, EaseInExpo, 0);
	TweenAddCallback(take_item, TWEEN_EV_FINISH, id, pocket_end);



}
