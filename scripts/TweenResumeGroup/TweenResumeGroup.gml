/// @description  TweenResumeGroup(group,deactivated)
/// @param group
/// @param deactivated
function TweenResumeGroup(argument0, argument1) {
	/*
	    @group       = tween group
	    @deactivated = affect tweens with deactivated targets?
    
	    RETURN:
	        NA
        
	    INFO:
	        Resumes all active tweens associated with specified tween group
	*/

	TweensExecute(TWEENS_GROUP, argument0, argument1, TweenResume);




}
