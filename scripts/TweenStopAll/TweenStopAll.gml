/// @description  TweenStopAll(deactivated)
/// @param deactivated
function TweenStopAll(argument0) {
	/*
	    @deactivated = affect tweens associated with deactivated targets?
    
	    RETURN:
	        NA
        
	    INFO:
	        Stops all active tweens
	*/

	TweensExecute(TWEENS_ALL, 0, argument0, TweenStop);





}
