/// @description  TweenGetExtData(tween)
/// @param tween
function TweenGetExtData(argument0) {

	var _t = TGMS_FetchTween(argument0);
	if (is_undefined(_t)) return 0;

	return _t[TWEEN.DATA];




}
