function closeup_shown() {
	shown = 1;
	var xp, yp, xoff, yoff;
	xoff = sprite_get_xoffset(spr_close_button);
	yoff = sprite_get_yoffset(spr_close_button);
	xp = min(bbox_right+xoff, GUI_W-xoff);
	yp = max(bbox_top-yoff, yoff);
	close_button = instance_create(xp, yp, obj_close_button);



}
