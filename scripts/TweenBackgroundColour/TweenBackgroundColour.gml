/// @description  TweenBackgroundColour(target,ease,mode,delta,delay,dur,col1,col2)
/// @param target
/// @param ease
/// @param mode
/// @param delta
/// @param delay
/// @param dur
/// @param col1
/// @param col2
function TweenBackgroundColour(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7) {

	return TweenFire(argument0, ext_background_colour__, argument1, argument2, argument3, argument4, argument5, argument6, argument7);





}
