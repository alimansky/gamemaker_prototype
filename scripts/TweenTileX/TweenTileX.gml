/// @description  TweenTileX(target,ease,mode,delta,delay,dur,tile,x1,x2)
/// @param target
/// @param ease
/// @param mode
/// @param delta
/// @param delay
/// @param dur
/// @param tile
/// @param x1
/// @param x2
function TweenTileX(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7, argument8) {

	return TweenFire(argument0, ext_tile_x__, argument1, argument2, argument3, argument4, argument5, argument7, argument8, argument6);




}
