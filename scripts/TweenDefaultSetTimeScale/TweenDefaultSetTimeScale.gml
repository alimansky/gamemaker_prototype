/// @description  TweenDefaultSetTimeScale(**Deprecated**scale)
/// @param **Deprecated**scale
function TweenDefaultSetTimeScale(argument0) {
	/*
	    Set default time scale assigned to newly created tweens
	*/

	global.TGMS_TweenDefault[@ TWEEN.TIME_SCALE] = clamp(argument0, 0, 100);




}
