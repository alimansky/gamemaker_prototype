/// @description  TweenCallbackInvalidate(callback)
/// @param callback
function TweenCallbackInvalidate(argument0) {
	/*
	    @callback = callback id
    
	    RETURN:
	        na
        
	    INFO:
	        Removes the callback from it's relevant tween event
        
	    Example:
	        // Create tween and add callback to finish event
	        tween = TweenCreate(id);
	        cb = TweenEventAddCallback(tween, TWEEN_EV_FINISH, id, ShowMessage, "Finished!");
        
	        // Invalidate callback -- effectively removes it from tween event
	        TweenInvalidate(cb);
	*/

	// Get callback
	var _cb = global.TGMS_MAP_CALLBACK[? argument0];

	if (is_array(_cb))
	{
	    // Invalidate callback from global id map
	    ds_map_delete(global.TGMS_MAP_CALLBACK, argument0);
	    // Set target as noone -- used for system cleaning
	    _cb[@ TWEEN_CALLBACK.TARGET] = noone;
	    // Nullify self reference handle
	    _cb[@ TWEEN_CALLBACK.ID] = 0;
	}




}
