// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function snd_play_ext(snd, pitch)
{
	if pitch == undefined pitch = random_range(0.9, 1.1);
	audio_sound_pitch(snd, pitch);
	audio_play_sound(snd, 1, 0);
	//audio_play_sound_at(snd, x, y, 0, point_distance(room_width/2, room_height/2, x, y), 1920, 1, 0, 0);
}