/// @description  TweenSetTime(tween,time)
/// @param tween
/// @param time
function TweenSetTime(argument0, argument1) {

	var _t = TGMS_FetchTween(argument0);
	if (is_undefined(_t)) return 0;

	// Assign new time value
	_t[@ TWEEN.TIME] = argument1;
	TweenForcePropertyUpdate(_t);




}
