/// @description  TweenGetDelta(tween)
/// @param tween
function TweenGetDelta(argument0) {

	var _t = TGMS_FetchTween(argument0);
	if (is_undefined(_t)) return false;

	return _t[TWEEN.DELTA];




}
