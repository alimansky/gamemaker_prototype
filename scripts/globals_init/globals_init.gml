function globals_init() 
{
	switch os_type
	{
	    case os_ios:
	        global.tilt_sign = 1;
	    break  
    
	    case os_android:
	        global.tilt_sign = -1;
	    break; 
    
	    default: global.tilt_sign = -1;
	}

	TweenSimpleUseDelta(1);
	global.sel = -1;
	global.closeup = -1;
	global.inv_list = ds_list_create();

	globalvar PAUSE;
	PAUSE = 0;
	
	// loc init
	loc_init();	
	
	// loc load
	loc_load(global.language, "loc//loc.tsv");
	
	set_font();
}
