/// @description  TweenStopGroup(group,deactivated)
/// @param group
/// @param deactivated
function TweenStopGroup(argument0, argument1) {
	/*
	    @group       = tween group
	    @deactivated = affect tweens associated with deactivated targets?
    
	    RETURN:
	        NA
        
	    INFO:
	        Stops all active tweens associated with specified tween group
	*/

	TweensExecute(TWEENS_GROUP, argument0, argument1, TweenStop);




}
