/// @description  TweenReverseGroup(group,deactivated)
/// @param group
/// @param deactivated
function TweenReverseGroup(argument0, argument1) {
	/*
	    @group       = tween group
	    @deactivated = affect tweens associated with deactivated targets?
    
	    RETURN:
	        NA
        
	    INFO:
	        Reverses tweens associated with specified tween group
	*/

	TweensExecute(TWEENS_GROUP, argument0, argument1, TweenReverse);




}
