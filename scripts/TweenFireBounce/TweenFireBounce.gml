/// @description  TweenFireBounce(**Deprecated**target,property,delta,ease,start,dest,dur)
/// @param **Deprecated**target
/// @param property
/// @param delta
/// @param ease
/// @param start
/// @param dest
/// @param dur
function TweenFireBounce(argument0, argument1, argument2, argument3, argument4, argument5, argument6) {

	return TweenFire(argument0, argument1, argument3, TWEEN_MODE_BOUNCE, argument2, 0, argument6, argument4, argument5);



}
