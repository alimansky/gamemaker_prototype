/// @description  TweenCallbackIsEnabled(callback)
/// @param callback
function TweenCallbackIsEnabled(argument0) {
	/*
	    @callback = callback id
    
	    RETURN:
	        bool
        
	    INFO:
	        Returns whether or not a specified callback is enabled
	*/

	var _cb = global.TGMS_MAP_CALLBACK[? argument0];
	if (is_undefined(_cb)) { return false; }

	return _cb[TWEEN_CALLBACK.ENABLED];



}
