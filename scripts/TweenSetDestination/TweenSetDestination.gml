/// @description  TweenSetDestination(tween,destination)
/// @param tween
/// @param destination
function TweenSetDestination(argument0, argument1) {

	var _t = TGMS_FetchTween(argument0);
	if (is_undefined(_t)) return 0;

	_t[@ TWEEN.CHANGE] = argument1 -_t[TWEEN.START];





}
