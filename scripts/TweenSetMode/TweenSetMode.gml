/// @description  TweenSetMode(tween,mode)
/// @param tween
/// @param mode
function TweenSetMode(argument0, argument1) {
 
	var _t = TGMS_FetchTween(argument0);
	if (is_undefined(_t)) return 0;

	_t[@ TWEEN.MODE] = argument1;




}
