function ps_init() {
	global.ps = part_system_create_layer("Particles", false);
	
	global.ps_fog = part_system_create_layer("Fog", false);
	global.pt_fog = geon_effect_add("effects/pt_fog.geon",global.ps_fog);
	global._pt_wrong_move = geon_effect_add("effects/pt_wrong_move.geon", global.ps, [spr_wrong_move]);
	
	
	//global.ps = part_system_create();
	//part_system_depth(global.ps, -99);

	//wrong_move
	/*
	global.pt_wrong_move = part_type_create();
	part_type_shape(global.pt_wrong_move, pt_shape_pixel);
	part_type_sprite(global.pt_wrong_move, spr_wrong_move, 0, 0, 0);
	part_type_size(global.pt_wrong_move, 1, 1, 0.02, 0);
	part_type_scale(global.pt_wrong_move, 1, 1);
	part_type_orientation(global.pt_wrong_move, 0, 360, 0, 0, 0);
	part_type_color3(global.pt_wrong_move, 16777215, 16777215, 16777215);
	part_type_alpha3(global.pt_wrong_move, 0.10, 1, 0);
	part_type_blend(global.pt_wrong_move, 0);
	part_type_life(global.pt_wrong_move, 30, 30);
	part_type_speed(global.pt_wrong_move, 5, 10, 0, 0);
	part_type_direction(global.pt_wrong_move, 60, 120, 0, 10);
	part_type_gravity(global.pt_wrong_move, 0.50, 270);
	*/

	//right_move	
	global._pt_right_move = geon_effect_add("effects/pt_goodjob.geon", global.ps, [spr_GoodJob]);
	
	//pickable object
	global._pt_pickitem = geon_effect_add("effects/pt_pickable.geon", global.ps, [spr_pickitem]);
}
