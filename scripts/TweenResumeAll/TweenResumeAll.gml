/// @description  TweenResumeAll(deactivated)
/// @param deactivated
function TweenResumeAll(argument0) {
	/*
	    @deactivated = affect tweens with deactivated targets?
    
	    RETURN:
	        NA
        
	    INFO:
	        Resumes all active tweens
	*/

	TweensExecute(TWEENS_ALL, 0, argument0, TweenResume);




}
