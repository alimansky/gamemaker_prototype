/// @description  TweenDefaultGetTimeScale(**Deprecated**)
/// @param **Deprecated**
function TweenDefaultGetTimeScale() {
	/*
	    Get default time scale assigned to newly created tweens
	*/

	return global.TGMS_TweenDefault[TWEEN.TIME_SCALE];




}
