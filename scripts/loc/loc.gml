// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
/*
	<English tag="en"/>
    <Russian tag="ru"/>
    <Japanese tag="jp"/>
    <German tag="de"/>
    <Italian tag="it"/>
    <Portuguese tag="por"/>
    <Chinese tag="ch"/>
    <Spanish tag="es"/>
    <French tag="fr"/>
    <Korean tag="kor"/>
*/
function loc_init() 
{	
	enum lang
	{
	    en,
	    ru,
		jp,
		de,
		it,
		por,
		ch,
		es,
		fr,
		kor
	}

	globalvar LOC_DATA;
	LOC_DATA = -1;
	
	global.language = lang.en;
}

///loc_load(language);
/// @param language
function loc_load(language, loc_file)
{
	SETTINGS_LANGUAGE_TEXT = language;
	if (LOC_DATA > 0 && ds_exists(LOC_DATA, ds_type_map))
		ds_map_destroy(LOC_DATA);
	LOC_DATA = tsv_to_loc_data(loc_file, SETTINGS_LANGUAGE_TEXT);
}

/// @param ID
function loc(argument0) 
{
	var value = LOC_DATA[?argument0];
	if is_undefined(value) value = argument0;
	return value;
}
