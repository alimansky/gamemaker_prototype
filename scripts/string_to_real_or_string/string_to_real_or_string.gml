///string_to_real_or_string(string);
function string_to_real_or_string(argument0) {
	/***************************************************
	  Checks if the input is a number or a text-string
	  and returns either a real or a string accordingly
	 ***************************************************/
	/// @arg str
	var str = argument0;
	var len, i, char, table, pos, has_digits;
	len = string_length( str );

	// проверяем есть ли в строке символы
	if len == 0 return str;

	// проверяем есть ли цифры в строке
	has_digits = false;
	i = -1;
	while ++i < 10
	{
	    pos = string_pos( string( i ), str );
	    if pos > 0
	    {
	        has_digits = true;
	        break;
	    }
	}
	if !has_digits return str;

	// проверяем состоит ли строка только из допустимых символов
	table = "0123456789-.";
	i = 0;
	while i++ < len
	{
	    char = string_char_at( str, i );
	    pos = string_pos(char,table);
	    if pos < 11
	    {
	        has_digits = true;
	    }
	    if pos == 0
	    {
	        return str;
	    }
	}
	return real(str);




}

