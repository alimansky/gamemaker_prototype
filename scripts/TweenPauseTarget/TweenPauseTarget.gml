/// @description  TweenPauseTarget(target,deactivated)
/// @param target
/// @param deactivated
function TweenPauseTarget(argument0, argument1) {
	/*
	    @target = target instance id or object index
    
	    RETURN:
	        NA
        
	    INFO:
	        Pauses all active tweens associated with specified target
	*/

	TweensExecute(TWEENS_GROUP, argument0, argument1, TweenPause);



}
