/// @description  TweenViewAngle(target,ease,mode,delta,delay,dur,view,angle1,angle2)
/// @param target
/// @param ease
/// @param mode
/// @param delta
/// @param delay
/// @param dur
/// @param view
/// @param angle1
/// @param angle2
function TweenViewAngle(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7, argument8) {

	return TweenFire(argument0, ext_view_angle__, argument1, argument2, argument3, argument4, argument5, argument7, argument8, argument6);





}
