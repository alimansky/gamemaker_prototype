/// @description  TweenGetDelayStart(tween)
/// @param tween
function TweenGetDelayStart(argument0) {

	var _t = TGMS_FetchTween(argument0);
	if (is_undefined(_t)) return 0;

	return _t[TWEEN.DELAY_START];



}
