// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function sequence_fire(seq_asset){
	var _seq = layer_sequence_create(layer, xstart, ystart, seq_asset);
	var _sq = sequence_get(seq_asset);
	var _obj = sequence_get_objects(_sq);
	var _seq_inst = layer_sequence_get_instance(_seq); 
	sequence_instance_override_object(_seq_inst, _obj[0], id);
}