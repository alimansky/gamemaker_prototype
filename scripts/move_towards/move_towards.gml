function move_towards(argument0, argument1, argument2) {
	var current, target, sp;
	current = argument0;
	target = argument1;
	sp = argument2;

	if (abs(target - current) <= sp)
	        return target;
	    else
	        return current + sign(target - current) * sp;



}
