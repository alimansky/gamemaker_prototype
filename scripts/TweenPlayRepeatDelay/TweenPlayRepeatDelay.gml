/// @description  TweenPlayRepeatDelay(**Deprecated**tween,delay[,ease,start,dest,dur])
/// @param **Deprecated**tween
/// @param delay[
/// @param ease
/// @param start
/// @param dest
/// @param dur]
function TweenPlayRepeatDelay() {

	var _t = TGMS_FetchTween(argument[0]);
	if (is_undefined(_t)) { return 0; }
    
	if (argument_count == 2)
	{    
	    return TweenPlay(_t, _t[TWEEN.PROPERTY_RAW], _t[TWEEN.EASE], TWEEN_MODE_REPEAT, _t[TWEEN.DELTA], argument[1], _t[TWEEN.DURATION], _t[TWEEN.START], _t[TWEEN.START]+_t[TWEEN.CHANGE]);
	}
	else
	{
	    return TweenPlay(_t, _t[TWEEN.PROPERTY_RAW], argument[2], TWEEN_MODE_REPEAT, _t[TWEEN.DELTA], argument[1], argument[5], argument[3], argument[4]);   
	}



}
