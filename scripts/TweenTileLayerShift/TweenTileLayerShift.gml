/// @description  TweenTileLayerShift(target,ease,mode,delta,delay,dur,depth,x,y)
/// @param target
/// @param ease
/// @param mode
/// @param delta
/// @param delay
/// @param dur
/// @param depth
/// @param x
/// @param y
function TweenTileLayerShift(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7, argument8) {

	return TweenFire(argument0, ext_tilelayer_shift__, argument1, argument2, argument3, argument4, argument5, 0, 1, argument6, argument7, argument8, 0);



}
