/// @description  TweenDefaultGetGroup(**Deprecated**)
/// @param **Deprecated**
function TweenDefaultGetGroup() {
	/*
	    Get default group assigned to newly created tweens
	*/

	return global.TGMS_TweenDefault[TWEEN.GROUP];




}
