/// @description  TweenIteratorReset(**Deprecated**iterator)
/// @param **Deprecated**iterator
function TweenIteratorReset(argument0) {

	var _tweens = SharedTweener().tweens;

	argument0[@ 0] = -1; // cached tween handle
	argument0[@ 1] = -1; // iterator index
	argument0[@ 2] = ds_list_size(_tweens); // tween list size
	argument0[@ 3] = _tweens; // cache tweens list



}
