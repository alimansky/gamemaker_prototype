/// @description  TweenFireRepeatDelay(**Deprecated**target,property,delta,ease,start,dest,dur,delay);
/// @param **Deprecated**target
/// @param property
/// @param delta
/// @param ease
/// @param start
/// @param dest
/// @param dur
/// @param delay
function TweenFireRepeatDelay(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7) {

	return TweenFire(argument0, argument1, argument3, TWEEN_MODE_REPEAT, argument2, argument7, argument6, argument4, argument5);



}
