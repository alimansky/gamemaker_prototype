function return_item(argument0) {
	trigger = argument0;
	global.sel = -1;
	in_pocket = 0;
	picked = 2;
	remove_from_inv(0);
	x = __view_get( e__VW.XView, 0 ) + x;
	y = __view_get( e__VW.YView, 0 ) + y;
	var tween = TweenSimpleScale(image_xscale, image_yscale, 0.3, 0.3, 0.25, EaseLinear);
	TweenSimpleMove(x, y, trigger.x, trigger.y, 0.25, EaseLinear);
	TweenAddCallback(tween, TWEEN_EV_FINISH, id, item_returned, 0);



}
