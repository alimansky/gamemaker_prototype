/// @description  TweenArray1D(target,ease,mode,delta,delay,dur,array,index,start,dest)
/// @param target
/// @param ease
/// @param mode
/// @param delta
/// @param delay
/// @param dur
/// @param array
/// @param index
/// @param start
/// @param dest
function TweenArray1D(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7, argument8, argument9) {

	return TweenFire(argument0, ext_Array1D__, argument1, argument2, argument3, argument4, argument5, argument8, argument9, argument6, argument7);




}
