/// @description  TweenBackgroundScaleY(target,ease,mode,delta,delay,dur,bg,y1,y2)
/// @param target
/// @param ease
/// @param mode
/// @param delta
/// @param delay
/// @param dur
/// @param bg
/// @param y1
/// @param y2
function TweenBackgroundScaleY(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7, argument8) {

	return TweenFire(argument0, ext_background_yscale__, argument1, argument2, argument3, argument4, argument5, argument7, argument8, argument6);




}
