/// @description  ext_background_blend__(amount,data[bg|col1|col2])
/// @param amount
/// @param data[bg|col1|col2]
function ext_background_blend__(argument0, argument1) {

	__background_set( e__BG.Blend, argument1[0], merge_colour(argument1[1], argument1[2], argument0) );




}
