function hide_closeup() {
	shown = 2;
	TweenSimpleMove(x, y, xstart, ystart, 0.5, EaseInExpo);
	var t_scale = TweenSimpleScale(image_xscale, image_yscale, scale_init, scale_init, 0.5, EaseInExpo);
	TweenFire(id, ext_alpha__, EaseInOutSine, TWEEN_MODE_ONCE, 1, 0, 0.5, alpha, 0);
	TweenAddCallback(t_scale, TWEEN_EV_FINISH, id, closeup_hidden);



}
