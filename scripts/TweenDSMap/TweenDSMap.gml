/// @description  TweenDSMap(target,ease,mode,delta,delay,dur,map,key,start,dest)
/// @param target
/// @param ease
/// @param mode
/// @param delta
/// @param delay
/// @param dur
/// @param map
/// @param key
/// @param start
/// @param dest
function TweenDSMap(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7, argument8, argument9) {

	return TweenFire(argument0, ext_DSMap__, argument1, argument2, argument3, argument4, argument5, argument8, argument9, argument6, argument7);




}
