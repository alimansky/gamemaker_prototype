/// @description  TweenGetStart(tween)
/// @param tween
function TweenGetStart(argument0) {

	var _t = TGMS_FetchTween(argument0);
	if (is_undefined(_t)) return 0;

	return _t[TWEEN.START];




}
