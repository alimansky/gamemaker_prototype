/// @description  TweenReverseTarget(target,deactivated)
/// @param target
/// @param deactivated
function TweenReverseTarget(argument0, argument1) {
	/*
	    @target      = target instance id or object index
	    @deactivated = affect tweens associated with deactivated targets?
    
	    RETURN:
	        NA
        
	    INFO:
	        Reverses play direction of tweens associated with target
	*/

	TweensExecute(TWEENS_TARGET, argument0, argument1, TweenReverse);



}
