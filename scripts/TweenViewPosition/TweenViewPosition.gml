/// @description  TweenViewPosition(target,ease,mode,delta,delay,dur,view,x1,y1,x2,y2)
/// @param target
/// @param ease
/// @param mode
/// @param delta
/// @param delay
/// @param dur
/// @param view
/// @param x1
/// @param y1
/// @param x2
/// @param y2
function TweenViewPosition(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7, argument8, argument9, argument10) {

	return TweenFire(argument0, ext_view_xy__, argument1, argument2, argument3, argument4, argument5, 0, 1, argument6, argument7, argument9, argument8, argument10);




}
