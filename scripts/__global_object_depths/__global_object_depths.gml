function __global_object_depths() {
	// Initialise the global array that allows the lookup of the depth of a given object
	// GM2.0 does not have a depth on objects so on import from 1.x a global array is created
	// NOTE: MacroExpansion is used to insert the array initialisation at import time
	gml_pragma( "global", "__global_object_depths()");

	// insert the generated arrays here
	global.__objectDepths[0] = 10000000; // obj_con
	global.__objectDepths[1] = 0; // obj_folder
	global.__objectDepths[2] = 0; // obj_gun
	global.__objectDepths[3] = 0; // obj_id
	global.__objectDepths[4] = 0; // obj_key_glove_compartment
	global.__objectDepths[5] = 0; // obj_key_start
	global.__objectDepths[6] = 0; // obj_lighter
	global.__objectDepths[7] = 0; // obj_glove_compartment
	global.__objectDepths[8] = 0; // obj_lighter_slot
	global.__objectDepths[9] = 0; // obj_key_hole
	global.__objectDepths[10] = 0; // obj_light_button
	global.__objectDepths[11] = 0; // obj_air_freshener
	global.__objectDepths[12] = 0; // obj_leaflets
	global.__objectDepths[13] = 0; // obj_paper_cup
	global.__objectDepths[14] = 0; // obj_stuff
	global.__objectDepths[15] = 0; // obj_steering_wheel
	global.__objectDepths[16] = 0; // obj_mirror
	global.__objectDepths[17] = 0; // obj_pedal
	global.__objectDepths[18] = -5; // obj_notebook_closeup
	global.__objectDepths[19] = 0; // obj_sticky_note_closeup
	global.__objectDepths[20] = -5; // obj_car_radio_closeup
	global.__objectDepths[21] = -6; // obj_case_number
	global.__objectDepths[22] = -5; // obj_case_closeup
	global.__objectDepths[23] = 0; // obj_notebook
	global.__objectDepths[24] = 0; // obj_car_radio
	global.__objectDepths[25] = 0; // obj_sticky_note
	global.__objectDepths[26] = 0; // obj_case
	global.__objectDepths[27] = 0; // obj_pickable
	global.__objectDepths[28] = 0; // obj_usable
	global.__objectDepths[29] = 0; // obj_trigger
	global.__objectDepths[30] = 0; // obj_prop
	global.__objectDepths[31] = 0; // obj_clickable
	global.__objectDepths[32] = 0; // obj_gui_button_con
	global.__objectDepths[33] = -5; // obj_closeup
	global.__objectDepths[34] = 0; // obj_SharedTweener
	global.__objectDepths[35] = 0; // obj_inventory
	global.__objectDepths[36] = -4; // obj_map_button
	global.__objectDepths[37] = -7; // obj_close_button
	global.__objectDepths[38] = -4; // obj_hint_button
	global.__objectDepths[39] = -3; // obj_left_arrow_button
	global.__objectDepths[40] = -3; // obj_right_arrow_button
	global.__objectDepths[41] = -99; // obj_exit_arrow


	global.__objectNames[0] = "obj_con";
	global.__objectNames[1] = "obj_folder";
	global.__objectNames[2] = "obj_gun";
	global.__objectNames[3] = "obj_id";
	global.__objectNames[4] = "obj_key_glove_compartment";
	global.__objectNames[5] = "obj_key_start";
	global.__objectNames[6] = "obj_lighter";
	global.__objectNames[7] = "obj_glove_compartment";
	global.__objectNames[8] = "obj_lighter_slot";
	global.__objectNames[9] = "obj_key_hole";
	global.__objectNames[10] = "obj_light_button";
	global.__objectNames[11] = "obj_air_freshener";
	global.__objectNames[12] = "obj_leaflets";
	global.__objectNames[13] = "obj_paper_cup";
	global.__objectNames[14] = "obj_stuff";
	global.__objectNames[15] = "obj_steering_wheel";
	global.__objectNames[16] = "obj_mirror";
	global.__objectNames[17] = "obj_pedal";
	global.__objectNames[18] = "obj_notebook_closeup";
	global.__objectNames[19] = "obj_sticky_note_closeup";
	global.__objectNames[20] = "obj_car_radio_closeup";
	global.__objectNames[21] = "obj_case_number";
	global.__objectNames[22] = "obj_case_closeup";
	global.__objectNames[23] = "obj_notebook";
	global.__objectNames[24] = "obj_car_radio";
	global.__objectNames[25] = "obj_sticky_note";
	global.__objectNames[26] = "obj_case";
	global.__objectNames[27] = "obj_pickable";
	global.__objectNames[28] = "obj_usable";
	global.__objectNames[29] = "obj_trigger";
	global.__objectNames[30] = "obj_prop";
	global.__objectNames[31] = "obj_clickable";
	global.__objectNames[32] = "obj_gui_button_con";
	global.__objectNames[33] = "obj_closeup";
	global.__objectNames[34] = "obj_SharedTweener";
	global.__objectNames[35] = "obj_inventory";
	global.__objectNames[36] = "obj_map_button";
	global.__objectNames[37] = "obj_close_button";
	global.__objectNames[38] = "obj_hint_button";
	global.__objectNames[39] = "obj_left_arrow_button";
	global.__objectNames[40] = "obj_right_arrow_button";
	global.__objectNames[41] = "obj_exit_arrow";


	// create another array that has the correct entries
	var len = array_length_1d(global.__objectDepths);
	global.__objectID2Depth = [];
	for( var i=0; i<len; ++i ) {
		var objID = asset_get_index( global.__objectNames[i] );
		if (objID >= 0) {
			global.__objectID2Depth[ objID ] = global.__objectDepths[i];
		} // end if
	} // end for


}
