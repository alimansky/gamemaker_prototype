/// @description  TweenCallbackEnable(callback,enable)
/// @param callback
/// @param enable
function TweenCallbackEnable(argument0, argument1) {
	/*
	    @callback = callback id
	    @enable   = enable callback execution?
    
	    RETURN:
	        na
        
	    INFO:
	        Allows specified callbacks to be enabled/disabled
	*/


	var _cb = global.TGMS_MAP_CALLBACK[? argument0];
	if (is_undefined(_cb)) { return false; }

	_cb[@ TWEEN_CALLBACK.ENABLED] = argument1;





}
