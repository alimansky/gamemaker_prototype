/// @description  TweenSetTimeScaleTarget(target,scale,deactivated)
/// @param target
/// @param scale
/// @param deactivated
function TweenSetTimeScaleTarget(argument0, argument1, argument2) {

	TweensExecute(TWEENS_TARGET, argument0, argument2, TweenSetTimeScale, argument1);



}
