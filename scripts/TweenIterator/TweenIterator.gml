/// @description  TweenIterator(**Deprecated**)
/// @param **Deprecated**
function TweenIterator() {
	/*
	    RETURN:
	        iterator
        
	    INFO:
	        Used to iterate through system's active tween list
        
	        I think this kind of stinks right now...
	        It might be useful, but I'll be looking into a better/faster solution.

	    EXAMPLE:
	        var _iterator = TweenIterator()
	        while(TweenIteratorNext(_iterator))
	        {
	            var _tween = _iterator[0];
	            TweenDoSomething(_tween);
	        }
	*/

	var _tweens = SharedTweener().tweens;
	var _iterator;

	_iterator[0] = -1; // cached tween handle
	_iterator[1] = -1; // iterator index
	_iterator[2] = ds_list_size(_tweens); // tween list size
	_iterator[3] = _tweens; // cache tweens list

	return _iterator;




}
