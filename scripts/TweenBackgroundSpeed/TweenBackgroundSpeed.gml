/// @description  TweenBackgroundSpeed(target,ease,mode,delta,delay,dur,bg,h1,v1,h2,v2)
/// @param target
/// @param ease
/// @param mode
/// @param delta
/// @param delay
/// @param dur
/// @param bg
/// @param h1
/// @param v1
/// @param h2
/// @param v2
function TweenBackgroundSpeed(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7, argument8, argument9, argument10) {

	return TweenFire(argument0, ext_background_hvspeed__, argument1, argument2, argument3, argument4, argument5, argument6, argument7, argument9, argument8, argument10);




}
