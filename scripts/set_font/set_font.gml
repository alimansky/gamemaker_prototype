// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function set_font()
{
	if(global.language == lang.jp)
		global.fnt_menu = font_add("font//NotoSansJP-Bold.ttf", 30, true, false, 0, 255);
	else if(global.language == lang.kor)
		global.fnt_menu = font_add("font//NotoSansKR-Bold.ttf", 30, true, false, 0, 255);
	else if(global.language == lang.ch)
		global.fnt_menu = font_add("font//NotoSansSC-Bold.ttf", 30, true, false, 0, 255);		
	else
		global.fnt_menu = font_add("font//8428523.ttf", 30, true, false, 0, 255);
}