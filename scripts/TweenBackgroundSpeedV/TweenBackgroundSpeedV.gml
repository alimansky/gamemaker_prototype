/// @description  TweenBackgroundSpeedV(target,ease,mode,delta,delay,dur,bg,v1,v2)
/// @param target
/// @param ease
/// @param mode
/// @param delta
/// @param delay
/// @param dur
/// @param bg
/// @param v1
/// @param v2
function TweenBackgroundSpeedV(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7, argument8) {

	return TweenFire(argument0, ext_background_vspeed__, argument1, argument2, argument3, argument4, argument5, argument7, argument8, argument6);



}
