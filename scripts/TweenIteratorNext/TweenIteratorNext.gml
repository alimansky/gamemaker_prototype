/// @description  TweenIteratorNext(**Deprecated**iterator)
/// @param **Deprecated**iterator
function TweenIteratorNext(argument0) {

	var _iterator = argument0;

	while(true)
	{
	    _iterator[@ 1] += 1;
    
	    if (_iterator[1] < _iterator[2])
	    {
	        var _t = ds_list_find_value(_iterator[3], _iterator[1]);
        
	        if (instance_exists(_t[TWEEN.TARGET]))
	        {
	            _iterator[@ 0] = _t[TWEEN.ID];
	            return true;
	        }
        
	        instance_activate_object(_t[TWEEN.TARGET]);
        
	        if (instance_exists(_t[TWEEN.TARGET]))
	        {
	            instance_deactivate_object(_t[TWEEN.TARGET]);
	            _iterator[@ 0] = _t[TWEEN.ID];
	            return true;
	        }
	    }
	    else
	    {
	        return false;
	    }
	}




}
