 function show_closeup() 
 {
	var obj = -1;
	var obj_name = object_get_name(object_index);
	var ind = asset_get_index(obj_name + "_closeup");
	if ind > -1
	{
	    if object_exists(ind) obj = ind;
	}

	if obj < 0 exit;

	var xp, yp;
	xp = x-__view_get( e__VW.XView, 0 );//device_mouse_x_to_gui(0);
	yp = y-__view_get( e__VW.YView, 0 );//device_mouse_y_to_gui(0);

	var ins = instance_create(xp, yp, obj);

	with ins
	{
	    image_xscale = other.sprite_width / sprite_width; //0.3;
	    image_yscale = image_xscale; //0.3;
	    trigger = other.id;
	    shown = 0;
	    scale_init = image_xscale;
	    TweenSimpleMove(xstart, ystart, GUI_W/2, GUI_H/2, 0.5, EaseOutExpo);

		var t_scale;
		if (room == rm_car_interior)
			t_scale = TweenSimpleScale(image_xscale, image_yscale, 1.7, 1.7, 0.5, EaseOutExpo);		
		else
			t_scale = TweenSimpleScale(image_xscale, image_yscale, 1, 1, 0.5, EaseOutExpo);
	    
	    TweenFire(id, ext_alpha__, EaseInOutSine, TWEEN_MODE_ONCE, 1, 0, 0.5, 0, 0.7);
	    TweenAddCallback(t_scale, TWEEN_EV_FINISH, id, closeup_shown);
	}

	global.closeup = ins;
	PAUSE = 1;
	visible = 0;
}