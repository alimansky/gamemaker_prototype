///@param x
///@param y
///@param object
function position_get_top_instance(xx, yy, object) {
	var _list = ds_list_create();
	var _num = instance_position_list(xx, yy, object, _list, false);
	if _num > 0
    {
		var top = noone;
		for (var i = 0; i < _num; ++i;)
        {
			if _list[| i] > top top = _list[| i];
        }
		
		var ins = top;
		ds_list_destroy(_list);
		return ins;
    }
	else
	{	
		ds_list_destroy(_list);
		return noone;
	}
}
