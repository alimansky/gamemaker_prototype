/// @description  TweenPlayLoop(**Deprecated**tween,[ease,start,dest,dur])
/// @param **Deprecated**tween
/// @param [ease
/// @param start
/// @param dest
/// @param dur]
function TweenPlayLoop() {

	var _t = TGMS_FetchTween(argument[0]);
	if (is_undefined(_t)) { return 0; }

	if (argument_count == 1)
	{
	    TweenSetMode(_t, TWEEN_MODE_LOOP);
	    TweenPlay(_t);
	}
	else
	{
	    TweenPlay(_t, _t[TWEEN.PROPERTY_RAW], argument[1], TWEEN_MODE_LOOP, _t[TWEEN.DELTA], 0, argument[4], argument[2], argument[3]); 
	}




}
