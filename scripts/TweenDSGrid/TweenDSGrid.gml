/// @description  TweenDSGrid(target,ease,mode,delta,delay,dur,grid,x,y,start,dest)
/// @param target
/// @param ease
/// @param mode
/// @param delta
/// @param delay
/// @param dur
/// @param grid
/// @param x
/// @param y
/// @param start
/// @param dest
function TweenDSGrid(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7, argument8, argument9, argument10) {

	return TweenFire(argument0, ext_DSGrid__, argument1, argument2, argument3, argument4, argument5, argument9, argument10, argument6, argument7, argument8);




}
