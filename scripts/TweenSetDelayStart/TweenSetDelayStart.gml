/// @description  TweenSetDelayStart(tween,delay)
/// @param tween
/// @param delay
function TweenSetDelayStart(argument0, argument1) {

	var _t = TGMS_FetchTween(argument0);
	if (is_undefined(_t)) return 0;

	_t[@ TWEEN.DELAY_START] = argument1;



}
