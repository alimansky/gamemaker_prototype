function scene_read(argument0) {
	var fname = argument0;
	if !file_exists(fname) {show_message("No File!"); exit;}

	var file = file_text_open_read(fname);
	if file = -1 {show_message("Can't open the file!"); exit;}

	var str, row, name, xp, yp, dp;

	dp = 0;
	while !file_text_eof(file)
	{
	    str = file_text_read_string(file);
	    if str != "" 
	    {
	        name = string_extract(str, "," , 0);
	        xp = string_extract(str, "," , 1);
	        yp = string_extract(str, "," , 2);
	        obj_add(name, xp, yp, dp);
	        dp += 1;
	    }
	    file_text_readln(file);
	}
	file_text_close(file);



}
