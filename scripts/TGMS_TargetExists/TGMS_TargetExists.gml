/// @description  TGMS_TargetExists(target,deactivated)
/// @param target
/// @param deactivated
function TGMS_TargetExists(argument0, argument1) {

	if (argument1)
	{
	    if (instance_exists(argument0)) { return true; }

	    instance_activate_object(argument0);
    
	    if (instance_exists(argument0))
	    {
	        instance_deactivate_object(argument0);
	        return true;
	    }
	}

	return (instance_number(argument0));




}
