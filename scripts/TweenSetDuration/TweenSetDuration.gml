/// @description  TweenSetDuration(tween,duration)
/// @param tween
/// @param duration
function TweenSetDuration(argument0, argument1) {

	var _t = TGMS_FetchTween(argument0);
	if (is_undefined(_t)) return 0;
    
	_t[@ TWEEN.DURATION] = argument1;




}
