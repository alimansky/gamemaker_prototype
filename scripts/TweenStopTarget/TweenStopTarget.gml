/// @description  TweenStopTarget(target,deactivated)
/// @param target
/// @param deactivated
function TweenStopTarget(argument0, argument1) {
	/*
	    @target      = target instance id or object index
	    @deactivated = include tweens associated with deactivated targets? 
    
	    RETURN:
	        NA
        
	    INFO:
	        Stops all active tweens associated with specified target
	*/

	TweensExecute(TWEENS_TARGET, argument0, argument1, TweenStop);



}
