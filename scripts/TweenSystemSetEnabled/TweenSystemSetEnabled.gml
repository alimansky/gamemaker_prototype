/// @description  TweenSystemSetEnabled(enable)
/// @param enable
function TweenSystemSetEnabled(argument0) {
	/*
	    @enable = enable tweening system?
    
	    RETURN:
	        na
        
	    INFO:
	        Used to enable/disable the tweening system
	*/

	(SharedTweener()).isEnabled = argument0;
	global.TGMS_IsEnabled = argument0;




}
