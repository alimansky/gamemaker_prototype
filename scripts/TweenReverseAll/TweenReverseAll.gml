/// @description  TweenReverseAll(deactivated)
/// @param deactivated
function TweenReverseAll(argument0) {
	/*
	    @deactivated = affect tweens associated with deactivated targets?
    
	    RETURN:
	        NA
        
	    INFO:
	        Reverses play direction of all active tweens
	*/

	TweensExecute(TWEENS_ALL, 0, argument0, TweenReverse);




}
