/// @description  TweenPlayTarget(target,deactivated)
/// @param target
/// @param deactivated
function TweenPlayTarget(argument0, argument1) {
	/*
	    @target      = instance id or object index
	    @deactivated = affect tweens with deactivated targets? 
    
	    RETURN:
	        NA
        
	    INFO:
	        Plays tweens associated with specfied target instance or object index.
	        All relevant tweens must be fully defined.
	*/

	TweensExecute(TWEENS_ALL, argument0, argument1, TweenPlay);




}
