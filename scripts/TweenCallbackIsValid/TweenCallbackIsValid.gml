/// @description  TweenCallbackIsValid(callback)
/// @param callback
function TweenCallbackIsValid(argument0) {
	/*
	    @callback = callback id
    
	    RETURN:
	        bool
        
	    INFO:
	        Returns whether or not the callback is valid (exists)
        
	    Example:
	        if (TweenCallbackValid(callback))
	        {
	            TweenCallbackInvalidate(callback);
	        }
	*/

	if (argument0)
	{
	    var _cb = global.TGMS_MAP_CALLBACK[? argument0];
	    if (is_undefined(_cb)) { return false; }
    
	    return TGMS_TargetExists(_cb[TWEEN_CALLBACK.TARGET], true);
	}





}
