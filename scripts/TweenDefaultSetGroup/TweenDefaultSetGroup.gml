/// @description  TweenDefaultSetGroup(**Deprecated**group)
/// @param **Deprecated**group
function TweenDefaultSetGroup(argument0) {
	/*
	    Set default group assigned to newly created tweens
	*/

	global.TGMS_TweenDefault[@ TWEEN.GROUP] = argument0;




}
