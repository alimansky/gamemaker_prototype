/// @description  TweenViewPortY(target,ease,mode,delta,delay,dur,view,y1,y2)
/// @param target
/// @param ease
/// @param mode
/// @param delta
/// @param delay
/// @param dur
/// @param view
/// @param y1
/// @param y2
function TweenViewPortY(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7, argument8) {

	return TweenFire(argument0, ext_view_port_y__, argument1, argument2, argument3, argument4, argument5, argument7, argument8, argument6);



}
