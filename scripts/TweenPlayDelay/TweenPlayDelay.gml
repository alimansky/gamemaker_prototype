/// @description  TweenPlayDelay(tween,delay)
/// @param tween
/// @param delay
function TweenPlayDelay(argument0, argument1) {

	var _t = TGMS_FetchTween(argument0);
	if (is_undefined(_t)) { return undefined; }

	_t[@ TWEEN.DELAY_START] = argument1;
	TweenPlay(_t);



}
