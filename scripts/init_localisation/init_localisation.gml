enum language
{
	en,
	ru, 
	de,
	fr,
	es
}

// default language
global.language = language.en;

function init_localisation()
{
	// play, settings, exit, music, enabled, disabled, volume, language, english, close

	for(var i = 0; i < 10; i++)
	{
	  for(var j = 0; j < 4; j ++)
	  {
	    global.localisation_words[i, j] = "";
	  }
	}

	// en
	global.localisation_words[0, 0] = "PLAY";
	global.localisation_words[0, 1] = "SETTINGS";
	global.localisation_words[0, 2] = "EXIT";
	global.localisation_words[0, 3] = "MUSIC";
	global.localisation_words[0, 4] = "ENABLED";
	global.localisation_words[0, 5] = "DISABLED";
	global.localisation_words[0, 6] = "VOLUME";
	global.localisation_words[0, 7] = "LANGUAGE";
	global.localisation_words[0, 8] = "ENGLISH";
	global.localisation_words[0, 9] = "CLOSE";
	// ru
	global.localisation_words[1, 0] = "ИГРАТЬ";
	global.localisation_words[1, 1] = "НАСТРОЙКИ";
	global.localisation_words[1, 2] = "ВЫХОД";
	global.localisation_words[1, 3] = "МУЗЫКА";
	global.localisation_words[1, 4] = "ВКЛЮЧЕНА";
	global.localisation_words[1, 5] = "ВЫКЛЮЧЕНА";
	global.localisation_words[1, 6] = "ГРОМКОСТЬ";
	global.localisation_words[1, 7] = "ЯЗЫК";
	global.localisation_words[1, 8] = "РУССКИЙ";
	global.localisation_words[1, 9] = "ЗАКРЫТЬ";
	// de
	global.localisation_words[2, 0] = "ABSPIELEN";
	global.localisation_words[2, 1] = "DIE EINSTELLUNGEN";
	global.localisation_words[2, 2] = "AUSFAHRT";
	global.localisation_words[2, 3] = "MUSIK";
	global.localisation_words[2, 4] = "AKTIVIERT";
	global.localisation_words[2, 5] = "BEHINDERT";
	global.localisation_words[2, 6] = "VOLUMEN";
	global.localisation_words[2, 7] = "SPRACHE";
	global.localisation_words[2, 8] = "DEUTSCHE";
	global.localisation_words[2, 9] = "SCHLIESSEN";
	// fr
	global.localisation_words[3, 0] = "JOUER";
	global.localisation_words[3, 1] = "RÉGLAGES";
	global.localisation_words[3, 2] = "SORTIR";
	global.localisation_words[3, 3] = "MUSIQUE";
	global.localisation_words[3, 4] = "ACTIVÉ";
	global.localisation_words[3, 5] = "DÉSACTIVÉE";
	global.localisation_words[3, 6] = "LE VOLUME";
	global.localisation_words[3, 7] = "LANGUE";
	global.localisation_words[3, 8] = "FRANÇAIS";
	global.localisation_words[3, 9] = "FERMER";
	// es
	global.localisation_words[3, 0] = "TOCAR";
	global.localisation_words[3, 1] = "AJUSTES";
	global.localisation_words[3, 2] = "SALIDA";
	global.localisation_words[3, 3] = "MÚSICA";
	global.localisation_words[3, 4] = "ACTIVADO";
	global.localisation_words[3, 5] = "DISCAPACITADO";
	global.localisation_words[3, 6] = "VOLUMEN";
	global.localisation_words[3, 7] = "IDIOMA";
	global.localisation_words[3, 8] = "ESPAÑOL";
	global.localisation_words[3, 9] = "CERRAR";
}