/// @description  TweenSetTimeScaleGroup(group,scale,deactivated)
/// @param group
/// @param scale
/// @param deactivated
function TweenSetTimeScaleGroup(argument0, argument1, argument2) {

	TweensExecute(TWEENS_GROUP, argument0, argument2, TweenSetTimeScale, argument1);




}
