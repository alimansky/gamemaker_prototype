/// @description  TweenSetDelay(tween,delay)
/// @param tween
/// @param delay
function TweenSetDelay(argument0, argument1) {

	var _t = TGMS_FetchTween(argument0);
	if (is_undefined(_t)) return 0;

	if (_t[TWEEN.DELAY] > 0) { _t[@ TWEEN.DELAY] = argument1; }




}
