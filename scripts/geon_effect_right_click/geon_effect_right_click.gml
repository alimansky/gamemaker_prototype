// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function geon_effect_right_click()
{
	//Generated for GMS2 in Geon FX v1.3
	//Put this code in Create event

	//NewEffect Particle System
	global.ps = part_system_create();
	part_system_depth(global.ps, -1);

	//NewEffect Particle Types
	//geon_good_job
	global.pt_geon_good_job = part_type_create();
	part_type_shape(global.pt_geon_good_job, pt_shape_pixel);
	//spr_3435184 = sprite_add("3435184.png", 1, 0, 0, 64, 64);
	part_type_sprite(global.pt_geon_good_job, spr_GoodJob, 1, 0, 0);
	part_type_size(global.pt_geon_good_job, 1, 1, 0, 0);
	part_type_scale(global.pt_geon_good_job, 1, 1);
	part_type_orientation(global.pt_geon_good_job, 0, 0, 0, 0, 0);
	part_type_color3(global.pt_geon_good_job, 16777215, 4235519, 255);
	part_type_alpha3(global.pt_geon_good_job, 1, 0.50, 0);
	part_type_blend(global.pt_geon_good_job, 0);
	part_type_life(global.pt_geon_good_job, 80, 80);
	part_type_speed(global.pt_geon_good_job, 5, 5, 0, 0);
	part_type_direction(global.pt_geon_good_job, 0, 360, 0, 0);
	part_type_gravity(global.pt_geon_good_job, 0, 0);

	//NewEffect Emitters
	global.pe_geon_good_job = part_emitter_create(global.ps);

	//NewEffect emitter positions. Streaming or Bursting Particles.
	var xp, yp;
	xp = x;
	yp = y;
	part_emitter_region(global.ps, global.pe_geon_good_job, xp-46, xp+46, yp-25, yp+25, ps_shape_diamond, ps_distr_linear);
	part_emitter_burst(global.ps, global.pe_geon_good_job, global.pt_geon_good_job, 10);

	//Destroying Emitters
	//part_emitter_destroy(global.ps, global.pe_geon_good_job);
}