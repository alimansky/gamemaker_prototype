function draw_sprite_tiled_hor(spr, xx, yy) {
	/***************************************************
	  Usage :   draw_sprite_tiled_hor(back,x,y)
	  Arguments :   argument0   ==  background
	                argument1   ==  x
	                argument2   ==  y
	  Event :   draw GUI event
	 ***************************************************/
 
	 var width, i;
 
	 width = sprite_get_width(spr);

	 for (i = -1; i < GUI_W / width + 1; i += 1)
	 {
	    draw_sprite(spr, 0, xx mod width + width * i, yy);
	 }
 



}
