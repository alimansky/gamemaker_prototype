/// @description  EaseInOutCubic(time,start,change,duration)
/// @param time
/// @param start
/// @param change
/// @param duration
function EaseInOutCubic(argument0, argument1, argument2, argument3) {

	_arg0 = 2*argument0/argument3;

	if (_arg0 < 1){
	   return argument2 * 0.5 * power(_arg0, 3) + argument1;
	}

	return argument2 * 0.5 * (power(_arg0 - 2, 3) + 2) + argument1;




}
