/// @description  TweenSystemClearRoom(room)
/// @param room
function TweenSystemClearRoom(argument0) {
	/*
	    @room = persistent room
    
	    RETURN:
	        na

	    INFO:
	        Clears persistent room's stored tween data
	*/

	TGMS_ClearRoom(argument0);




}
