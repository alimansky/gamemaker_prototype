/// @description  TweenSetGroup(tween,group)
/// @param tween
/// @param group
function TweenSetGroup(argument0, argument1) {

	var _t = TGMS_FetchTween(argument0);
	if (is_undefined(_t)) return 0;

	_t[@ TWEEN.GROUP] = argument1;





}
