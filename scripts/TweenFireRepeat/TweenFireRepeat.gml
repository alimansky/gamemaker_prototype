/// @description  TweenFireRepeat(**Deprecated**target,property,delta,ease,start,dest,dur)
/// @param **Deprecated**target
/// @param property
/// @param delta
/// @param ease
/// @param start
/// @param dest
/// @param dur
function TweenFireRepeat(argument0, argument1, argument2, argument3, argument4, argument5, argument6) {

	return TweenFire(argument0, argument1, argument3, TWEEN_MODE_REPEAT, argument2, 0, argument6, argument4, argument5);




}
