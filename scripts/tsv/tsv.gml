// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
///tsv_string_to_list(str)
/// @param string
function tsv_string_to_list(argument0) {
	/***************************************************
	  Takes a string with tab seperated values and returns the value in ds_list
	 ***************************************************/
	var str, length, value, quote_column, i, char, list;

	str = argument0;
	list = ds_list_create();

	length = string_length(str);
	value = "";
	for (i = 1; i <= length; i += 1) //Loop through string adding one character at a time to value, adding the value to the list at every seperator
	{ 
	    char = string_char_at(str, i);
	    if char == "\t"
	    {
	        value = string_to_real_or_string(value);
	        ds_list_add(list, value);
	        value = "";
	    };
	    else value += char;
	};
	value = string_to_real_or_string(value);
	ds_list_add(list, value);

	return list;
}

///tsv_to_loc_data(file, lang)
/// @param file
/// @param lang
function tsv_to_loc_data(argument0, argument1) {

	var file = argument0;
	var lang = argument1;
	if !file_exists(file) {show_error("No Loc Data!", 0); exit;}

	file = file_text_open_read(file);
	if file = -1 {show_error("Can't open the file!", 0); exit;}

	var str, row;
	var rows_list = ds_list_create();
	while !file_text_eof(file)
	{
	    str = file_text_read_string(file);		
	    if str != ""
	    {
	        row = tsv_string_to_list(str);//string_parse_ext(str, ",", true);
	        ds_list_add(rows_list, row);
	    }
	    file_text_readln(file);
	}
	file_text_close(file);

	var first_row = ds_list_find_value(rows_list, 0);
	if !ds_exists(first_row, ds_type_list) exit;

	var w = ds_list_size(first_row);
	var h = ds_list_size(rows_list);

	var xx, yy, value, map;
	map = ds_map_create();
	for (yy = 1; yy < h; yy += 1)
	{
	    row = ds_list_find_value(rows_list, yy);
	    ds_map_add(map, ds_list_find_value(row, 0), ds_list_find_value(row, 1+lang)); 
	    ds_list_destroy(row);
	}

	ds_list_destroy(first_row);
	ds_list_destroy(rows_list);
	//show_message('Parse Complete!#' + string( argument0 ) + '#Items: ' + string( ds_map_size(map) ) );
	//show_message(json_encode(map));
	return map;
}

