function gui_button_check() {
	mx = device_mouse_x_to_gui(0);//GUI_W * (device_mouse_raw_x(0) / DW);
	my = device_mouse_y_to_gui(0);//GUI_H * (device_mouse_raw_y(0) / DH);
	hover = position_get_top_instance(mx, my, obj_gui_button_con); //point_in_rectangle(mx, my, bbox_left, bbox_top, bbox_right, bbox_bottom);

	if hover != noone
	{
	    if mouse_check_button_pressed(mb_left)
	    {
	        with hover
	        {
	            if pressed == 0 
	            {
	                pressed = 1;
	                global.sel = id;
	                //TAP SOUND
	            }
	        }
	    }
    
	    if !mouse_check_button(mb_left) 
	    {    
	        with hover
	        {
	            if pressed
	            {
	                event_perform(ev_other, ev_user0);
	                pressed = 0;   
	                if global.sel = id global.sel = -1;
	            }
	        }
	    }
	}



}
