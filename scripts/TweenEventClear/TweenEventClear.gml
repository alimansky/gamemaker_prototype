/// @description  TweenEventClear(tween,event)
/// @param tween
/// @param event
function TweenEventClear(argument0, argument1) {
	/*
	    @tween = tween id
	    @event = tween event constant -- TWEEN_EV_****
    
	    RETURN:
	        NA
        
	    INFO:
	        Removes all callbacks from the specified tween event
	*/

	var _t = TGMS_FetchTween(argument0);
	if (is_undefined(_t)) { return 0; }

	var _events = _t[TWEEN.EVENTS];

	if (_events != -1)
	{    
	    if (ds_map_exists(_events, argument1))
	    {
	        var _event = _events[? argument1]; 
	        var _index = 0;
        
	        repeat(ds_list_size(_event)-1)
	        {
	            // Get callback
	            var _cb = _event[| ++_index];
	            // Delete global id
	            ds_map_delete(global.TGMS_MAP_CALLBACK, _cb[TWEEN_CALLBACK.ID]);
	            // Invalidate target
	            _cb[@ TWEEN_CALLBACK.TARGET] = noone;
	            // Nullify self reference id
	            _cb[@ TWEEN_CALLBACK.ID] = 0;
	        }
	    }
	}




}
